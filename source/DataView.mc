using Toybox.WatchUi as UI;
using Toybox.Graphics as Gfx;
using Toybox.System;
using Toybox.Application as App;

class DataView {

    var dataX;
    var dataY;

    var font;
    
    var left;
    var right;
    
    var foregroundColour;
    
    var time;
    var distance;
    var pace;
    
    hidden var FIELD_TIME = 0;
    hidden var FIELD_DISTANCE = 1;
    hidden var FIELD_PACE = 2;
    

    function initialize() {
    	updateSettings();
    }
    
    function updateSettings() {
    	var app = App.getApp();
    	font = getFont(getNumberProperty(app, "font_size", 2));
    	
    	System.println("foreground colour:");
    	foregroundColour = getColour(getNumberProperty(app, "foreground_colour", 1));
    	
    	left = getFieldType(getNumberProperty(app, "left_field", 0));
    	right = getFieldType(getNumberProperty(app, "right_field", 1));
    }

    function display(dc) {
        checkFont(dc);
        displayValues(dc);
    }

    function displayValues(dc) {
        dc.setColor(foregroundColour, Gfx.COLOR_TRANSPARENT);
        var linelength = 0;
		
        linelength = printText(getFieldText(left), linelength, dc, font);
        var verticalLineX;
        if (font == Gfx.FONT_NUMBER_THAI_HOT) {
        	verticalLineX = linelength + 1;
        }
        else {
        	verticalLineX = linelength + 12;
        	linelength += 4;
        }
        dc.drawLine(verticalLineX, 0, verticalLineX, dc.getHeight());
        linelength += 6;
        
        linelength = printText(getFieldText(right), linelength, dc, font);
        
    }
    
    function getFieldText(field_number) {
    	var text;
    	if (FIELD_TIME == field_number) {
    		text = getTimerString(time);
    	}
    	else if (FIELD_DISTANCE == field_number) {
    		text = "" + distance.format("%.3f");
    	}
    	else {
    		text = getTimerString(pace);
    	}  	
    	return text;
    }
    
    function getTimerString(time) {
    	var text;
    	if (0 == time || null == time) {
            text = "00:00";
        }
        else {
            text = minutesToTime(time);
        }
        return text;
    }
    
    function printText(text, linelength, dc, font) {
    	for (var i = 0; i < text.length(); i++ ) {
        	var character = text.substring(i, i+1);
        	dc.drawText(dataX + linelength, dataY, font, "" + character, Gfx.TEXT_JUSTIFY_LEFT);
        	var charWidth = dc.getTextWidthInPixels("" + character, font);
        	if (charWidth > 0) {
        		linelength += charWidth;
        		if (font == Gfx.FONT_NUMBER_THAI_HOT) {
        			linelength -= 5;
        			if (character.equals("1")) {
        				linelength -= 7;
        			}
        			else if (character.equals(":")) {
        				linelength += 1;
        			}
        			else if (character.equals(" ")) {
        				linelength += 4;
        			}
        		} 
        	}
        	
        }
        return linelength;
    }
    
    function minutesToTime(minutes) {
        //System.print("minutes = " + minutes);

        var wholeHours = (minutes/60).toNumber();

        var wholeMinutes = (minutes - (wholeHours * 60)).toNumber();
        //System.print("wholeMinutes = " + wholeMinutes);

        var seconds = (60 * (minutes - wholeMinutes - (wholeHours * 60))).toNumber();
        //System.println("seconds = " + seconds);

        var result = "";

        if (wholeHours > 0) {
            result += wholeHours + ":";
        }
        result += wholeMinutes.format("%02d") + ":" + seconds.format("%02d");

        return result;

    }

    function checkFont(dc) {
        if (font == Gfx.FONT_NUMBER_THAI_HOT) {
            dataX = - 3;
            dataY = - 5;
        }
        else if (font == Gfx.FONT_NUMBER_HOT) {
            dataX = 7;
        	if (55 < dc.getHeight()) {
            	dataY = 8;
        	}
        	else {
            	dataY = 1;
            }
        }
        else if (font == Gfx.FONT_NUMBER_MEDIUM) {
            dataX = 7;
        	if (55 < dc.getHeight()) {
            	dataY = 11;
        	}
        	else {
            	dataY = 4;
            }
        }
        else {
            dataX = 7;
        	if (55 < dc.getHeight()) {
            	dataY = 17;
        	}
        	else {
            	dataY = 9;
            }
        }
    }
    
    function getFont(font_size) {
    	System.println("font_size = " + font_size);
   		if (null == font_size) {
    		font_size = 1;
    	} 
    	if (0 == font_size) {
    		font = Gfx.FONT_NUMBER_MILD;
    	}
    	else if (1 == font_size) {
    		font = Gfx.FONT_NUMBER_MEDIUM;
    	}
    	else if (2 == font_size) {
    		font = Gfx.FONT_NUMBER_HOT;
    	}
    	else {
    		font = Gfx.FONT_NUMBER_THAI_HOT;
    	}
    	System.println("font = " + font);
    	return font;
    }
    
    function getColour(colour_number) {
    	System.println("colour_number = " + colour_number);
    	var colour;
    	if (null == colour_number || 0 == colour_number) {
    		colour = Gfx.COLOR_WHITE;
    	}
    	else if (1 == colour_number) {
    		colour = Gfx.COLOR_BLACK;
    	}
    	else if (2 == colour_number) {
    		colour = Gfx.COLOR_RED;
    	}
    	else {
    		colour = Gfx.COLOR_GREEN;
    	}
    	System.println("colour = " + colour);
    	return colour;
    }
    
    function getFieldType(field_number) {
    	System.println("field_number = " + field_number);
    	var field;
    	if (null == field_number || 0 == field_number) {
    		field = FIELD_TIME;
    	}
    	else if (1 == field_number) {
    		field = FIELD_DISTANCE;
    	}
    	else {
    		field = FIELD_PACE;
    	}
    	System.println("field = " + field);
    	return field;
    }
    
    function getNumberProperty(app, key, initial) {
        var value = app.getProperty(key);
        if (value != null) {

            if (value instanceof Lang.Number) {
                return value;
            }
            else if (value instanceof Lang.Float) {
                return value.toNumber();
            }
            else if (value instanceof Lang.String) {
                return value.toNumber();
            }
        }

        return initial;
	}

}
