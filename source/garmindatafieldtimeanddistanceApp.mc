using Toybox.Application as App;

class garmindatafieldtimeanddistanceApp extends App.AppBase {

	var view;

    //! onStart() is called on application start up
    function onStart() {
    }

    //! onStop() is called when your application is exiting
    function onStop() {
    }

    //! Return the initial view of your application here
    function getInitialView() {
    	if (null == view) {
    		view = new garmindatafieldtimeanddistanceView();
    	}
        return [ view ];
    }
    
    function onSettingsChanged() {
    	if (null != view) {
			view.updateSettings();
		}
		else {
			System.println("View was null");
		}
    }
    
    

}