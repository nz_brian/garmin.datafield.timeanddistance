using Toybox.WatchUi as UI;
using Toybox.System;
using Toybox.Lang;
using Toybox.Graphics as Gfx;
using Toybox.Application as App;

class garmindatafieldtimeanddistanceView extends UI.DataField {

    var datafield;
    var distanceConversion = 1;
    
    var backgroundColour;


    function initialize() {
        datafield = new DataView();

        //if watch set to miles, then convert distances to miles
        if (System.getDeviceSettings().distanceUnits == System.UNIT_STATUTE) {
            distanceConversion = 0.621371192237;
        }
        
        updateSettings();
    }
    
    function updateSettings() {
    	System.println("background colour:");
    	var colour_number = datafield.getNumberProperty(App.getApp(), "background_colour", 0);
    	backgroundColour = datafield.getColour(colour_number);
    	datafield.updateSettings();
    }

    function compute(info) {
        var currentDistance = info.elapsedDistance;
        //System.println("Current distance = " + currentDistance);
        if (null == currentDistance) {
            currentDistance = 0.0;
        }
        //System.println("Current distance = " + currentDistance);
        datafield.distance = (currentDistance / 1000).toFloat() * distanceConversion;

        var currentTime = info.timerTime;
        if (null == currentTime)
        {
            currentTime = 0.0;
        }
        // convert to minutes
        currentTime =  currentTime.toFloat() / 1000 / 60;
        datafield.time = currentTime;
        
        //var speed = info.currentSpeed; (meters per second);
        //var speedInUnitsPerMinute = 60 * speed * distanceConversion / 1000; (KMs or Miles)
        //var paceInMinutesPerUnit = 1 / speedInUnitsPerMinute;
		var currentSpeed = info.currentSpeed;
		if (null == currentSpeed) {
			datafield.pace = 0;
		}
		else {
        	datafield.pace = 1000 / (60 * currentSpeed * distanceConversion);
        }

        return 0;
    }

    function onUpdate(dc) {

        dc.setColor( backgroundColour, backgroundColour);
        dc.clear();

        datafield.display(dc);
        return true;
    }


}